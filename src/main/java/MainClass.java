import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class MainClass {

    private static ArrayList<EnterData> dataList = new ArrayList<EnterData>(
            Arrays.asList(
                    new EnterData("test1210@inbox.ru", "test1234", "SUCCESS_AUTH"),
                    new EnterData("", "", "SUCCESS_INVALID_DATA"),
                    new EnterData("test1210@inbox.ru", "","SUCCESS_INVALID_DATA"),
                    new EnterData("","test1234","SUCCESS_INVALID_DATA"),
                    new EnterData("fail","test1234","SUCCESS_INVALID_DATA"),
                    new EnterData("test1210@inbox.ru","fail","SUCCESS_INVALID_DATA")
            )
    );

    private static void enterData(String email, String password, WebDriver driver) {
        driver.get("https://realtimeboard.com/login/");

        WebElement emailElement = driver.findElement(By.id("email"));
        emailElement.clear();
        emailElement.click();
        emailElement.sendKeys(email);

        WebElement passwordElement = driver.findElement(By.id("password"));
        passwordElement.click();
        passwordElement.sendKeys(password);
        WebElement button = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div/div/form/button"));
        button.submit();
    }

    private static String successTest(WebDriver driver, String email, String password) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        String result = "ERROR TEST";

        try {
            driver.manage().window().maximize();
            enterData(email, password, driver);

            WebElement successElement = driver.findElement(By.xpath("/html/body/div[6]/div[2]/div[2]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/span"));
            if (successElement.getText().equals("All boards")) {
                result = "SUCCESS_AUTH";
            }
        } catch (NoSuchElementException error) {
            result = "SUCCESS_INVALID_DATA";
        }
        return result;
    }

    private static void chromeTest() {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

        for (EnterData enterData : dataList) {
            WebDriver chromeDriver = new ChromeDriver();
            String result = successTest(chromeDriver, enterData.email, enterData.password);
            if (enterData.result.equals(result)) {
                System.out.println("Test completed success in Chrome " + enterData.result);
            } else {
                System.out.println("Test failed in Chrome " + enterData.result);
            }
            chromeDriver.close();
        }
    }

    private static void firefoxTest() {
        System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");

        for (EnterData enterData : dataList) {
            WebDriver firefoxDriver = new FirefoxDriver();
            String result = successTest(firefoxDriver, enterData.email, enterData.password);
            if (enterData.result.equals(result)) {
                System.out.println("Test completed success in FireFox " + enterData.result);
            } else {
                System.out.println("Test failed in FireFox " + enterData.result);
            }
            firefoxDriver.close();
        }
    }

    public static void main(String[] args) {
        chromeTest();
        firefoxTest();
    }
}
