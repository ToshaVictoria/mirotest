public class EnterData {

    public String email;
    public String password;
    public String result;

    EnterData(String email, String password, String result){

        this.email = email;
        this.password = password;
        this.result = result;
    }
}
